package com.example.party;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

public class ArticleGenerator {
    @Nonnull
    private final String loremIpsum;

    public ArticleGenerator(Context context) {
        loremIpsum = context.getResources().getString(R.string.lorem);
    }

    @Nonnull
    public List<Article> generate(int count) {
        ArrayList<Article> result = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            result.add(next());
        }

        return result;
    }

    @NonNull
    private Article next() {
        return new Article(loremIpsum);
    }
}
